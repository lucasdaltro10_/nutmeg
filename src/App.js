import React, { Component } from "react";
import FeeCalculator from "./feeCalculator";

class App extends Component {
  render() {
    return (
      <div className="App">
        <FeeCalculator />
      </div>
    );
  }
}

export default App;
