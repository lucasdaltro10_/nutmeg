import React from "react";
import { shallow } from "enzyme";
import FeeCalculatorCard from "./FeeCalculatorCard";

describe("Fee Calculator card", () => {
  const wrapper = shallow(<FeeCalculatorCard />);
  const startingContributionInput = wrapper.find("#startContribution");
  const monthlyContributionInput = wrapper.find("#monthlyContribution");

  it("Renders properly", () => {
    expect(wrapper).toMatchSnapshot();
  });

  it("Calculates a fee correctly", () => {
    startingContributionInput.simulate("change", { target: { value: 500 } });
    monthlyContributionInput.simulate("change", { target: { value: 250 } });
    wrapper.find("form").simulate("submit", { preventDefault: jest.fn() });
    expect(wrapper.find("#totalFees").text()).toEqual("£108.57");
  });

  it("Validates against empty values", () => {
    startingContributionInput.simulate("change", { target: { value: "" } });
    monthlyContributionInput.simulate("change", { target: { value: "" } });
    expect(wrapper.find('[value="Calculate"]').props().disabled).toBe(true);
  });
});
