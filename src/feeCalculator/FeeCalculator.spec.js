import React from "react";
import { shallow } from "enzyme";
import FeeCalculator from "./index";

describe("Fee Calculator", () => {
  const wrapper = shallow(<FeeCalculator />);

  it("Renders properly", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
