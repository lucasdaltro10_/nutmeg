import React from "react";
import styles from "./FeeCalculator.module.css";
import FeeCalculatorCard from "./FeeCalculatorCard";

const FeeCalculator = () => {
  return (
    <>
      <h1 className={styles.title}>Fee charges</h1>
      <FeeCalculatorCard />
    </>
  );
};

export default FeeCalculator;
