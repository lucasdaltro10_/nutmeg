import React, { useState } from "react";
import styles from "./FeeCalculatorCard.module.css";
import calculateTotalFees from "../calculateTotalFees";

const MONTHS = 36;

const FORM_STATUS = {
  invalid: "INVALID",
  valid: "VALID",
  submitted: "SUBMITTED"
};

const FeeCalculatorCard = () => {
  const [startingContribution, setStartingContribution] = useState(0);
  const [monthlyContribution, setMonthlyContribution] = useState(0);
  const [formStatus, setFormStatus] = useState(FORM_STATUS.invalid);
  const [total, setTotal] = useState(0);

  const calculate = e => {
    e.preventDefault();
    if (formStatus === FORM_STATUS.valid) {
      setTotal(
        calculateTotalFees(
          startingContribution,
          monthlyContribution,
          MONTHS
        ).toFixed(2)
      );
      setFormStatus(FORM_STATUS.submitted);
    }
  };

  const changeValue = (value, setValue) => {
    setValue(parseFloat(value));
    if (!isNaN(value) && value > 0) {
      setFormStatus(FORM_STATUS.valid);
    } else {
      setFormStatus(FORM_STATUS.invalid);
    }
  };

  return (
    <div className={styles.container}>
      <h3 className={styles.title}>Set your amounts</h3>
      <form onSubmit={calculate}>
        <div className={styles.contributionInputContainer}>
          <div>
            <label className={styles.formLabel} htmlFor="startContribution">
              Starting contribution
            </label>
            {/* TODO: These should be using ::before instead */}
            <span className={styles.poundSign} aria-hidden="true">
              £
            </span>
            <input
              aria-label="Starting Contribution"
              aria-required="true"
              type="text"
              id="startContribution"
              className={styles.formInput}
              onChange={e =>
                changeValue(e.target.value, setStartingContribution)
              }
            />
          </div>
          <div>
            <label className={styles.formLabel} htmlFor="monthlyContribution">
              Monthly contribution
            </label>
            <span className={styles.poundSign} aria-hidden="true">
              £
            </span>
            <input
              type="text"
              aria-label="Monthly Contribution"
              aria-required="true"
              id="monthlyContribution"
              className={styles.formInput}
              onChange={e =>
                changeValue(e.target.value, setMonthlyContribution)
              }
            />
          </div>
        </div>
        {formStatus === FORM_STATUS.submitted && (
          <p>
            With a starting contribution of <b>£{startingContribution}</b> and a
            montly contribution of <b>£{monthlyContribution}</b> we will deduct
            a total of <b id="totalFees">£{total}</b> in a timeframe of{" "}
            <b>{MONTHS}</b> months.
          </p>
        )}
        <input
          type="submit"
          className={styles.calculatuBtn}
          value="Calculate"
          disabled={formStatus === FORM_STATUS.invalid}
        />
      </form>
    </div>
  );
};

export default FeeCalculatorCard;
