const FEE_CHARGE_PERCENTAGE = 0.0625;

const calculateTotalFees = (
  startingContribution,
  monthlyContribution,
  months
) => {
  let totalFees = 0;
  let totalOfThePot = startingContribution;
  for (let i = 1; i < months; i++) {
    totalOfThePot += monthlyContribution;
    const feeForThisMonth = totalOfThePot * (FEE_CHARGE_PERCENTAGE / 100);
    totalOfThePot -= feeForThisMonth;
    totalFees += feeForThisMonth;
  }
  return totalFees;
};

export default calculateTotalFees;
